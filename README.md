# M-A-pipeline-extension
Version 0.1

## Installation  
- clone the project from https://gitlab.com/Franci5co/m-a-pipeline-extension.git
- go to web: `chrome://extensions/`   
- switch on: `Developer Mode`  
- click `LOAD UNPACKED` button   
- search the folder and click `SELECT` button  
- open your app
