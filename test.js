const buttonEl = document.getElementsByClassName('merge-button')[0];
buttonEl.disabled = true;
const topBarEl = document.getElementsByClassName('pull-request-actions')[0];


// Text
const textEl = document.createElement('a');
topBarEl.appendChild(textEl);
textEl.innerHTML ='Pipeline verified';
textEl.setAttribute('href', 'https://teamcity.apps.global.rakuten.com/viewType.html?buildTypeId=MiniApps_MiniAppsEditor_Standard');
textEl.setAttribute('target', '_blank');
textEl.style.top = '59px';
textEl.style.right = '91px';
textEl.style.color = 'red';
textEl.style.textDecoration = 'underline';
textEl.style.position = 'absolute';

// Input
const inputEl = document.createElement('input');
topBarEl.appendChild(inputEl);
inputEl.type = 'checkbox';
inputEl.style.position = 'absolute';
inputEl.style.top = '60px';
inputEl.style.right = '66px';

// Listener
inputEl.addEventListener('change', function() {
    if(this.checked) {
        buttonEl.disabled = false;
        textEl.style.color = 'black';
    } else {
        buttonEl.disabled = true;
        textEl.style.color = 'red';
    }

});

